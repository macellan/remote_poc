KEYS = {8: 'BackSpace',
        9: 'Tab',
        13: 'KP_Enter',
        20: 'Caps_Lock',
        27: 'Escape',
        32: 'space',
        33: 'Page_Up',
        34: 'Page_Down',
        35: 'End',
        36: 'Home',
        37: 'Left',
        38: 'Up',
        39: 'Right',
        40: 'Down',
        45: 'Insert',
        46: 'Delete',
        190: 'period',
        188: 'comma',
        }

MKEYS = {
        16: 'shift',
        17: 'ctrl',
        18: 'alt',
        }

MARKS = {
        188: 44,
    }
