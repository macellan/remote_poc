import web
import sys
import gtk.gdk
import cStringIO as StringIO 
import base64 
import subprocess

from fix import *

render = web.template.render('templates/')

urls = (
    '/', 'index',
    '/get-image', 'getImage',
    '/mousemove?', 'mousemove',
    '/mousedown?', 'mousedown',
    '/mouseup?', 'mouseup',
    '/keystroke?', 'keystroke',
)

app = web.application(urls, globals())

class index:
    def GET(self):
        return render.index()

class getImage:
    def GET(self):
        io = StringIO.StringIO()
        w = gtk.gdk.get_default_root_window()
        sz = w.get_size()
        pb = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,False,8,sz[0],sz[1])
        pb = pb.get_from_drawable(w,w.get_colormap(),0,0,0,0,sz[0],sz[1])
        pb.save_to_callback(io.write, 'jpeg', {'quality': '20'}) 
        return base64.b64encode(io.getvalue())


class mousemove:
    def GET(self):
        inp = web.input()
        subprocess.call(["xdotool", "mousemove", inp['x'], inp['y']])
        return ''


class mousedown:
    def GET(self):
        inp = web.input()
        subprocess.call(["xdotool", "mousedown", inp['button']])
        return ''

class mouseup:
    def GET(self):
        inp = web.input()
        subprocess.call(["xdotool", "mouseup", inp['button']])
        return ''

class keystroke:
    def GET(self):
        inp = web.input()
        print inp
        meta_keys = []
        keys = []
        chars = []
        marks = []
        for key in inp:
            key = int(key)
            if key in MKEYS:
                key = MKEYS[key]
                meta_keys.append(key)
                continue
            elif key in KEYS:
                key = KEYS[key]
                keys.append(key)
                continue
            elif key in MARKS:
                key = MARKS[key]
                marks.append(chr(key))
                continue
            else:
                key = chr(key)
                chars.append(key)
        keys_str = '+'.join(meta_keys)
        keys_str += '+'+'+'.join(keys)
        keys_str += '+'+'+'.join(chars)
        keys_str += '+'+'+'.join(marks)
        if not meta_keys and not keys:
            keys_str = '+'.join(chars) + '+'.join(marks)
        print 'key_str = ', keys_str
        subprocess.call(["xdotool", "key", keys_str])
        return ''

if __name__ == "__main__":
    app.run()
