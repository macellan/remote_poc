$(document).ready(
    function() 
    {
        var keys = {};
        var refreshIntervalId = setInterval(function() {
            $.get("/get-image", function(data){
                $('img').attr('src', 'data:image/png;base64, ' + data);
            });
        }, 100);
        
        $('body img.rdesktop').mousemove(
            function(e) {
                e.preventDefault();
                $.get("/mousemove", {'x': e.pageX-5, 
                                     'y': e.pageY-5});
            }
        );
        
        $('body img.rdesktop').mousedown(
            function(e) {
                e.preventDefault();
                console.log(e.which);
                $.get("/mousedown", {'button': e.which});
            }
        );
        
        $('body img.rdesktop').mouseup(
            function(e) {
                e.preventDefault();
                $.get("/mouseup", {'button': e.which});
            }
        );
        
        $('body img.rdesktop').bind('contextmenu', function(e) {
            return false;
        }); 
        
        $(window).keydown(
            function(e) {
                e.preventDefault();
                keys[e.which] = true;
                return false;
            }
        );
        
        $(window).keyup(
            function(e) {
                e.preventDefault();
                if ((keys.hasOwnProperty(16)) && (e.which >= 65 && e.which <= 97)) {
                    key = e.which+32;
                    tmp_keys = {}
                    tmp_keys[key] = true
                    $.get("/keystroke", tmp_keys);
                    delete tmp_keys;
                }
                else {
                    $.get("/keystroke", keys);
                }
                delete keys[e.which];
                return false;
            }
        );
    }
);
